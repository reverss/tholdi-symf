<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    #[Route('/profil', name:'app_profile')]
    public function index(EntityManagerInterface $entityManager) {

        $listReservations = $entityManager->getRepository(Reservation::class)->findByCodeClient($this->getUser()->getId());
        $cart = $entityManager->getRepository(Cart::class)->findByCodeClient($this->getUser()->getId());


        return $this->render('front/profil.html.twig', ['Reservations' => $listReservations, 'Paniers' => $cart]);
    }
}