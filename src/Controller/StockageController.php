<?php

namespace App\Controller;

use App\Entity\Container;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StockageController extends AbstractController
{
    #[Route('/stockage', name: 'app_stockage')]
    public function index(EntityManagerInterface $entityManager) {

        $containersList = $entityManager->getRepository(Container::class)->findAll();

        return $this->render('front/stockage.html.twig', ['ContainersList' => $containersList]);
    }
}