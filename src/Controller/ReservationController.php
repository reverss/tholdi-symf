<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\Container;
use App\Entity\Reservation;
use App\Entity\Reserver;
use App\Entity\Ville;
use Cassandra\Date;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ReservationController extends AbstractController
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    #[Route('/reservation', name: 'app_reservation')]
    public function reservation() {

        $listVilles = $this->entityManager->getRepository(Ville::class)->findAll();

        return $this->render('front/reservation.html.twig', ['Villes' => $listVilles]);

    }

    #[Route('/internal/reservation/add', name: 'internal_reservationadd')]
    public function reservationAdd(Request $request) {

        $reservation = new Reservation();

        $reservation->setCodeClient($this->getUser());

        $date = \DateTime::createFromFormat('m/d/Y', $request->get('dateDebutReservation'));
        $dateDebutReservation = $date->format('Y-m-d');

        $datetest = new \DateTimeImmutable($dateDebutReservation);

        $reservation->setDateDebutReservation($datetest);

        $date = \DateTime::createFromFormat('m/d/Y', $request->get('dateFinReservation'));
        $dateFinReservation = $date->format('Y-m-d');

        $datetest1 = new \DateTimeImmutable($dateFinReservation);

        $reservation->setDateFinReservation($datetest1);

        /** @var Ville $villeMiseDispo */
        $villeMiseDispo = $this->entityManager->getRepository(Ville::class)->findOneById($request->get('codeVilleMiseDispo'));

        /** @var Ville $villeRendre */
        $villeRendre = $this->entityManager->getRepository(Ville::class)->findOneById(($request->get('codeVilleRendre')));

        $reservation->setCodeVilleMiseDispo($villeMiseDispo);
        $reservation->setCodeVilleRendre($villeRendre);
        $reservation->setVolumeEstime($request->get('volumeEstime'));
        $reservation->setEtat('A');

        $this->entityManager->persist($reservation);
        $this->entityManager->flush($reservation);

        $cartList = $this->entityManager->getRepository(Cart::class)->findByCodeClient($this->getUser()->getId());

        /** @var Cart $cart */
        foreach ($cartList as $cart)
        {
            $reserver = new Reserver();

            $reserver->setCodeReservation($reservation);

            $container = $this->entityManager->getRepository(Container::class)->findOneById($cart->getTypeContainer()->getId());

            $reserver->setTypeContainer($container);

            $reserver->setQuantiteReserver(count($cartList));

            $this->entityManager->persist($reserver);
            $this->entityManager->flush($reserver);

        }

        $conn = $this->entityManager->getConnection();

        $conn->executeQuery('DELETE FROM cart WHERE code_client_id = :codeClient', [
            'codeClient' => $this->getUser()->getId()
        ]);

        return $this->redirectToRoute('app_profile');
    }
}