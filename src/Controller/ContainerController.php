<?php

namespace App\Controller;

use App\Entity\Container;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ContainerController extends AbstractController
{
    #[Route('/container', name: 'app_container')]
    public function index(Request $request, EntityManagerInterface $entityManager) {

        $idSlug = $request->get('slug');

        $container = $entityManager->getRepository(Container::class)->findOneByTypeContainer($idSlug);


        return $this->render('front/conteneur.html.twig', ['Container' => $container]);
    }
}