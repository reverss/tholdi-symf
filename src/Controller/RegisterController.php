<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Pays;
use App\Security\LoginAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class RegisterController extends AbstractController
{
    public function __construct(private EntityManagerInterface $entityManager,
                                private UserPasswordHasherInterface $passwordHasher,)
    {
    }


    #[Route('/register', name: 'app_register')]
    public function index(Request $request, UserAuthenticatorInterface $userAuthenticator, LoginAuthenticator $loginAuthenticator): Response
    {
        $pays = $this->entityManager->getRepository(Pays::class)->findAll();
        $errorsMail = [];
        $errorsPwd = [];

        if ($request->isMethod('POST'))
        {
            $raisonS = $request->get('raisonSociale');
            $login = $request->get('pseudo');
            $telephone = $request->get('telephone');
            $contact = $request->get('contact');
            $ville = $request->get('ville');
            $email = $request->get('email');
            $paysPost = $request->get('pays');
            /** @var Pays $paysId */
            $pays = $this->entityManager->getRepository(Pays::class)->findOneByCodePays($paysPost);
            $codeP = $request->get('cp');
            $adresse = $request->get('adresse');
            $pass = $request->get('password');
            $passC = $request->get('passwordC');

            if($email == "")
            {
                $errorsMail[] = "L'email est obligatoire !";
            }
            if(null !== $this->entityManager->getRepository(Client::class)->findOneByEmail($email))
            {
                $errorsMail[] = "L'email est déjà utilisé !";
            }
            if($pass != $passC || $passC == "")
            {
                $errorsPwd[] = "Les deux mot de passe ne correspondent pas !";
            }
            if (strlen($pass) < 8) {
                $errorsPwd[] = "8 caractères minium requis";
            }
            if (count($errorsPwd) == 0 && count($errorsMail) == 0)
            {
                $client = new Client();

                $client->setRaisonSociale($raisonS);
                $client->setLogin($login);
                $client->setAdresse($adresse);
                $client->setContact($contact);
                $client->setTelephone($telephone);
                $client->setVille($ville);
                $client->setCodePostal($codeP);
                $client->setEmail($email);
                $client->setCodePays($pays);
                $passHashed = $this->passwordHasher->hashPassword($client, $pass);
                $client->setPassword($passHashed);

                $this->entityManager->persist($client);
                $this->entityManager->flush($client);

                $userAuthenticator->authenticateUser($client, $loginAuthenticator, $request);
                return $this->redirectToRoute('app_home');
            }
        }
        return $this->render('front/register.html.twig', [
            'pays' => $pays,
            'errorsMail' => $errorsMail,
            'errorsPwd' => $errorsPwd
        ]);
    }
}
