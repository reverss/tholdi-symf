<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\Container;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    #[Route('/internal/cart/add', name: 'internal_cart_add')]
    public function cartAdd(Request $request) {

        $container = $this->entityManager->getRepository(Container::class)->findOneByTypeContainer($request->get('slug'));

        $user = $this->getUser();

        $cart = new Cart();

        $cart->setCodeClient($user);
        $cart->setTypeContainer($container);
        $cart->setQuantite($request->get('quantite'));

        $this->entityManager->persist($cart);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_stockage');
    }

    #[Route('/internal/cart/delete', name: 'internal_cart_cartdelete')]
    public function cartDelete(Request $request) {

        /** @var Container $container */
        $container = $this->entityManager->getRepository(Container::class)->findOneByTypeContainer($request->get('slug'));

        $conn = $this->entityManager->getConnection();

        $conn->executeQuery('DELETE FROM cart WHERE type_container_id = :typeContainer AND code_client_id = :codeClient', [
            'typeContainer' => $container->getId(),
            'codeClient' => $this->getUser()->getId()
        ]);

        return $this->redirectToRoute('app_profile');
    }
}