<?php

namespace App\Entity;

use App\Repository\ReserverRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReserverRepository::class)]
class Reserver
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'reservers')]
    private ?Reservation $codeReservation = null;

    #[ORM\ManyToOne(inversedBy: 'reservers')]
    private ?Container $typeContainer = null;

    #[ORM\Column]
    private ?int $quantiteReserver = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeReservation(): ?Reservation
    {
        return $this->codeReservation;
    }

    public function setCodeReservation(?Reservation $codeReservation): self
    {
        $this->codeReservation = $codeReservation;

        return $this;
    }

    public function getTypeContainer(): ?Container
    {
        return $this->typeContainer;
    }

    public function setTypeContainer(?Container $typeContainer): self
    {
        $this->typeContainer = $typeContainer;

        return $this;
    }

    public function getQuantiteReserver(): ?int
    {
        return $this->quantiteReserver;
    }

    public function setQuantiteReserver(int $quantiteReserver): self
    {
        $this->quantiteReserver = $quantiteReserver;

        return $this;
    }
}
