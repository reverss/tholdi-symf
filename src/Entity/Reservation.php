<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateDebutReservation = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $dateFinReservation = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $dateReservation = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 4, scale: '0')]
    private ?string $volumeEstime = null;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    private ?Client $codeClient = null;

    #[ORM\Column(length: 1)]
    private ?string $etat = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Devis $codeDevis = null;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    private ?Ville $codeVilleMiseDispo = null;

    #[ORM\ManyToOne(inversedBy: 'reservationsS')]
    private ?Ville $codeVilleRendre = null;

    #[ORM\OneToMany(mappedBy: 'codeReservation', targetEntity: Reserver::class)]
    private Collection $reservers;

    public function __construct()
    {
        $this->reservers = new ArrayCollection();
        $this->dateReservation = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebutReservation(): ?\DateTimeInterface
    {
        return $this->dateDebutReservation;
    }

    public function setDateDebutReservation(\DateTimeInterface $dateDebutReservation): self
    {
        $this->dateDebutReservation = $dateDebutReservation;

        return $this;
    }

    public function getDateFinReservation(): ?\DateTimeInterface
    {
        return $this->dateFinReservation;
    }

    public function setDateFinReservation(\DateTimeInterface $dateFinReservation): self
    {
        $this->dateFinReservation = $dateFinReservation;

        return $this;
    }

    public function getDateReservation(): ?\DateTimeInterface
    {
        return $this->dateReservation;
    }

    public function setDateReservation(\DateTimeInterface $dateReservation): self
    {
        $this->dateReservation = $dateReservation;

        return $this;
    }

    public function getVolumeEstime(): ?string
    {
        return $this->volumeEstime;
    }

    public function setVolumeEstime(string $volumeEstime): self
    {
        $this->volumeEstime = $volumeEstime;

        return $this;
    }

    public function getCodeClient(): ?Client
    {
        return $this->codeClient;
    }

    public function setCodeClient(?Client $codeClient): self
    {
        $this->codeClient = $codeClient;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getCodeDevis(): ?Devis
    {
        return $this->codeDevis;
    }

    public function setCodeDevis(?Devis $codeDevis): self
    {
        $this->codeDevis = $codeDevis;

        return $this;
    }

    public function getCodeVilleMiseDispo(): ?Ville
    {
        return $this->codeVilleMiseDispo;
    }

    public function setCodeVilleMiseDispo(?Ville $codeVilleMiseDispo): self
    {
        $this->codeVilleMiseDispo = $codeVilleMiseDispo;

        return $this;
    }

    public function getCodeVilleRendre(): ?Ville
    {
        return $this->codeVilleRendre;
    }

    public function setCodeVilleRendre(?Ville $codeVilleRendre): self
    {
        $this->codeVilleRendre = $codeVilleRendre;

        return $this;
    }

    /**
     * @return Collection<int, Reserver>
     */
    public function getReservers(): Collection
    {
        return $this->reservers;
    }

    public function addReserver(Reserver $reserver): self
    {
        if (!$this->reservers->contains($reserver)) {
            $this->reservers->add($reserver);
            $reserver->setCodeReservation($this);
        }

        return $this;
    }

    public function removeReserver(Reserver $reserver): self
    {
        if ($this->reservers->removeElement($reserver)) {
            // set the owning side to null (unless already changed)
            if ($reserver->getCodeReservation() === $this) {
                $reserver->setCodeReservation(null);
            }
        }

        return $this;
    }
}
