<?php

namespace App\Entity;

use App\Repository\ContainerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ContainerRepository::class)]
class Container
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 4)]
    private ?string $typeContainer = null;

    #[ORM\Column(length: 255)]
    private ?string $libelleTypeContainer = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: '0')]
    private ?string $longueurCont = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: '0')]
    private ?string $largeurCont = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: '0')]
    private ?string $hauteurCont = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 3, scale: '0')]
    private ?string $volume = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: '0')]
    private ?string $poidsCont = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 8, scale: 2)]
    private ?string $capaciteDeCharge = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    private ?string $tarifJour = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 6, scale: 2)]
    private ?string $tarifTrim = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 7, scale: 2)]
    private ?string $tarifAnn = null;

    #[ORM\Column(length: 255)]
    private ?string $photo = null;

    #[ORM\Column(length: 255)]
    private ?string $photo2 = null;

    #[ORM\Column(length: 255)]
    private ?string $photo3 = null;

    #[ORM\Column(length: 255)]
    private ?string $photo4 = null;

    #[ORM\OneToMany(mappedBy: 'typeContainer', targetEntity: Reserver::class)]
    private Collection $reservers;

    #[ORM\OneToMany(mappedBy: 'typeContainer', targetEntity: Cart::class)]
    private Collection $carts;

    public function __construct()
    {
        $this->reservers = new ArrayCollection();
        $this->carts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeContainer(): ?string
    {
        return $this->typeContainer;
    }

    public function setTypeContainer(string $typeContainer): self
    {
        $this->typeContainer = $typeContainer;

        return $this;
    }

    public function getLibelleTypeContainer(): ?string
    {
        return $this->libelleTypeContainer;
    }

    public function setLibelleTypeContainer(string $libelleTypeContainer): self
    {
        $this->libelleTypeContainer = $libelleTypeContainer;

        return $this;
    }

    public function getLongueurCont(): ?string
    {
        return $this->longueurCont;
    }

    public function setLongueurCont(string $longueurCont): self
    {
        $this->longueurCont = $longueurCont;

        return $this;
    }

    public function getLargeurCont(): ?string
    {
        return $this->largeurCont;
    }

    public function setLargeurCont(string $largeurCont): self
    {
        $this->largeurCont = $largeurCont;

        return $this;
    }

    public function getHauteurCont(): ?string
    {
        return $this->hauteurCont;
    }

    public function setHauteurCont(string $hauteurCont): self
    {
        $this->hauteurCont = $hauteurCont;

        return $this;
    }

    public function getVolume(): ?string
    {
        return $this->volume;
    }

    public function setVolume(string $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    public function getPoidsCont(): ?string
    {
        return $this->poidsCont;
    }

    public function setPoidsCont(string $poidsCont): self
    {
        $this->poidsCont = $poidsCont;

        return $this;
    }

    public function getCapaciteDeCharge(): ?string
    {
        return $this->capaciteDeCharge;
    }

    public function setCapaciteDeCharge(string $capaciteDeCharge): self
    {
        $this->capaciteDeCharge = $capaciteDeCharge;

        return $this;
    }

    public function getTarifJour(): ?string
    {
        return $this->tarifJour;
    }

    public function setTarifJour(string $tarifJour): self
    {
        $this->tarifJour = $tarifJour;

        return $this;
    }

    public function getTarifTrim(): ?string
    {
        return $this->tarifTrim;
    }

    public function setTarifTrim(string $tarifTrim): self
    {
        $this->tarifTrim = $tarifTrim;

        return $this;
    }

    public function getTarifAnn(): ?string
    {
        return $this->tarifAnn;
    }

    public function setTarifAnn(string $tarifAnn): self
    {
        $this->tarifAnn = $tarifAnn;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getPhoto2(): ?string
    {
        return $this->photo2;
    }

    public function setPhoto2(string $photo2): self
    {
        $this->photo2 = $photo2;

        return $this;
    }

    public function getPhoto3(): ?string
    {
        return $this->photo3;
    }

    public function setPhoto3(string $photo3): self
    {
        $this->photo3 = $photo3;

        return $this;
    }

    public function getPhoto4(): ?string
    {
        return $this->photo4;
    }

    public function setPhoto4(string $photo4): self
    {
        $this->photo4 = $photo4;

        return $this;
    }

    /**
     * @return Collection<int, Reserver>
     */
    public function getReservers(): Collection
    {
        return $this->reservers;
    }

    public function addReserver(Reserver $reserver): self
    {
        if (!$this->reservers->contains($reserver)) {
            $this->reservers->add($reserver);
            $reserver->setTypeContainer($this);
        }

        return $this;
    }

    public function removeReserver(Reserver $reserver): self
    {
        if ($this->reservers->removeElement($reserver)) {
            // set the owning side to null (unless already changed)
            if ($reserver->getTypeContainer() === $this) {
                $reserver->setTypeContainer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Cart>
     */
    public function getCarts(): Collection
    {
        return $this->carts;
    }

    public function addCart(Cart $cart): self
    {
        if (!$this->carts->contains($cart)) {
            $this->carts->add($cart);
            $cart->setTypeContainer($this);
        }

        return $this;
    }

    public function removeCart(Cart $cart): self
    {
        if ($this->carts->removeElement($cart)) {
            // set the owning side to null (unless already changed)
            if ($cart->getTypeContainer() === $this) {
                $cart->setTypeContainer(null);
            }
        }

        return $this;
    }
}
