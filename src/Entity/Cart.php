<?php

namespace App\Entity;

use App\Repository\CartRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CartRepository::class)]
class Cart
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'carts')]
    private ?Client $codeClient = null;

    #[ORM\ManyToOne(inversedBy: 'carts')]
    private ?Container $typeContainer = null;

    #[ORM\Column]
    private ?int $quantite = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeClient(): ?Client
    {
        return $this->codeClient;
    }

    public function setCodeClient(?Client $codeClient): self
    {
        $this->codeClient = $codeClient;

        return $this;
    }

    public function getTypeContainer(): ?Container
    {
        return $this->typeContainer;
    }

    public function setTypeContainer(?Container $typeContainer): self
    {
        $this->typeContainer = $typeContainer;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }
}
