<?php

namespace App\Entity;

use App\Repository\DevisRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DevisRepository::class)]
class Devis
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 5)]
    private ?string $codeDevis = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateDevis = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 6, scale: 2)]
    private ?string $montantDevis = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 4, scale: '0')]
    private ?string $volume = null;

    #[ORM\Column]
    private ?int $nbContainers = null;

    #[ORM\Column(length: 1)]
    private ?string $valider = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeDevis(): ?string
    {
        return $this->codeDevis;
    }

    public function setCodeDevis(string $codeDevis): self
    {
        $this->codeDevis = $codeDevis;

        return $this;
    }

    public function getDateDevis(): ?\DateTimeInterface
    {
        return $this->dateDevis;
    }

    public function setDateDevis(\DateTimeInterface $dateDevis): self
    {
        $this->dateDevis = $dateDevis;

        return $this;
    }

    public function getMontantDevis(): ?string
    {
        return $this->montantDevis;
    }

    public function setMontantDevis(string $montantDevis): self
    {
        $this->montantDevis = $montantDevis;

        return $this;
    }

    public function getVolume(): ?string
    {
        return $this->volume;
    }

    public function setVolume(string $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    public function getNbContainers(): ?int
    {
        return $this->nbContainers;
    }

    public function setNbContainers(int $nbContainers): self
    {
        $this->nbContainers = $nbContainers;

        return $this;
    }

    public function getValider(): ?string
    {
        return $this->valider;
    }

    public function setValider(string $valider): self
    {
        $this->valider = $valider;

        return $this;
    }
}
