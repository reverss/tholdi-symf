<?php

namespace ContainerOwO71b8;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_6ebY1LlService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.6ebY1Ll' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.6ebY1Ll'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'loginAuthenticator' => ['privates', 'App\\Security\\LoginAuthenticator', 'getLoginAuthenticatorService', true],
            'userAuthenticator' => ['privates', 'security.user_authenticator', 'getSecurity_UserAuthenticatorService', true],
        ], [
            'loginAuthenticator' => 'App\\Security\\LoginAuthenticator',
            'userAuthenticator' => '?',
        ]);
    }
}
