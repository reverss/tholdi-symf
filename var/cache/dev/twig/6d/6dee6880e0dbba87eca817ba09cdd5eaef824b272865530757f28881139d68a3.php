<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/home.html.twig */
class __TwigTemplate_53946d466b8d15de3af5efa867e6809ca47db0515b7c944f10aeefd2500c217c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/home.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "front/home.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"w-4/5 mx-auto mt-32\">
        <div class=\"grid grid-cols-2 items-center px-6 py-10 mx-auto space-y-6 lg:h-[32rem] lg:py-16 lg:flex-row lg:items-center\">
            <div class=\"animate-in slide-in-from-left duration-500 w-full lg:w-1/2\">
                <div class=\"ml-16 lg:max-w-lg\">
                    <h1 class=\"text-[40px] tracking-[1px] font-bold tracking-wide text-blue-400 lg:text-5xl\">THOLDI<span class=\"text-black\">, votre stockage prend vie en quelques clics !</span></h1>

                    <div class=\"mt-8 space-y-5 font-medium\">
                        <p class=\"flex items-center -mx-2 text-black\">
                            <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"w-6 h-6 mx-2 text-blue-500\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z\" />
                            </svg>
                            <span class=\"mx-2\">Rapide et efficace</span>
                        </p>

                        <p class=\"flex items-center -mx-2 text-black\">
                            <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"w-6 h-6 mx-2 text-blue-500\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z\" />
                            </svg>
                            <span class=\"mx-2\">Pas cher et sécurisé</span>
                        </p>

                        <p class=\"flex items-center -mx-2 text-black\">
                            <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"w-6 h-6 mx-2 text-blue-500\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z\" />
                            </svg>
                            <span class=\"mx-2\">Facile à réserver</span>
                        </p>
                        <div class=\"\">
                            <a href=\"\">
                                <div class=\"flex shadow-xl w-80 rounded-md p-2 hover:bg-blue-500 transition duration-500\">
                                    <div x-transition class=\"bg-blue-400 rounded-full\">
                                        <img class=\" mx-1\" src=\"https://www.resotainer.fr/assets/www/images/front/illustrations/solution-box-032f348a42.svg\" alt=\"\" width=\"64\" height=\"64\">
                                    </div>
                                    <span class=\"font-medium w-40 mx-6 my-auto text-black\">Un conteneur livré chez vous</span>
                                    <i class=\"float-right my-auto mr-2 fas fa-arrow-right\"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"animate-in slide-in-from-right duration-500\">
                <img class=\"object-cover w-full h-full mx-auto border-2 border-gray-200 rounded-2xl shadow-lg lg:max-w-2xl\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/static/img/container1.jpeg"), "html", null, true);
        echo "\" alt=\"\">
            </div>
        </div>
        <footer>

        </footer>
    </main>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "front/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 46,  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"w-4/5 mx-auto mt-32\">
        <div class=\"grid grid-cols-2 items-center px-6 py-10 mx-auto space-y-6 lg:h-[32rem] lg:py-16 lg:flex-row lg:items-center\">
            <div class=\"animate-in slide-in-from-left duration-500 w-full lg:w-1/2\">
                <div class=\"ml-16 lg:max-w-lg\">
                    <h1 class=\"text-[40px] tracking-[1px] font-bold tracking-wide text-blue-400 lg:text-5xl\">THOLDI<span class=\"text-black\">, votre stockage prend vie en quelques clics !</span></h1>

                    <div class=\"mt-8 space-y-5 font-medium\">
                        <p class=\"flex items-center -mx-2 text-black\">
                            <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"w-6 h-6 mx-2 text-blue-500\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z\" />
                            </svg>
                            <span class=\"mx-2\">Rapide et efficace</span>
                        </p>

                        <p class=\"flex items-center -mx-2 text-black\">
                            <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"w-6 h-6 mx-2 text-blue-500\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z\" />
                            </svg>
                            <span class=\"mx-2\">Pas cher et sécurisé</span>
                        </p>

                        <p class=\"flex items-center -mx-2 text-black\">
                            <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"w-6 h-6 mx-2 text-blue-500\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z\" />
                            </svg>
                            <span class=\"mx-2\">Facile à réserver</span>
                        </p>
                        <div class=\"\">
                            <a href=\"\">
                                <div class=\"flex shadow-xl w-80 rounded-md p-2 hover:bg-blue-500 transition duration-500\">
                                    <div x-transition class=\"bg-blue-400 rounded-full\">
                                        <img class=\" mx-1\" src=\"https://www.resotainer.fr/assets/www/images/front/illustrations/solution-box-032f348a42.svg\" alt=\"\" width=\"64\" height=\"64\">
                                    </div>
                                    <span class=\"font-medium w-40 mx-6 my-auto text-black\">Un conteneur livré chez vous</span>
                                    <i class=\"float-right my-auto mr-2 fas fa-arrow-right\"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"animate-in slide-in-from-right duration-500\">
                <img class=\"object-cover w-full h-full mx-auto border-2 border-gray-200 rounded-2xl shadow-lg lg:max-w-2xl\" src=\"{{ asset('build/static/img/container1.jpeg') }}\" alt=\"\">
            </div>
        </div>
        <footer>

        </footer>
    </main>
{% endblock %}", "front/home.html.twig", "/var/www/tholdiv2/templates/front/home.html.twig");
    }
}
