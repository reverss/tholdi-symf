<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/login.html.twig */
class __TwigTemplate_a565d091b510a7717586b10b584067731381eb70a8f2404e59bef30326b388cd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/login.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "front/login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"animate-in slide-in-from-right duration-700 absolute w-full top-52 items-center justify-center\">
        <div class=\"mx-auto my-auto bg-gray-100 text-gray-500 rounded-3xl shadow-xl overflow-hidden\" style=\"max-width:1000px\">
            <div class=\"md:flex w-full\">
                <div class=\"md:block w-1/2 py-10 px-10 bg-cover\" style=\"background-image: url('";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/static/img/connexion1.jpg"), "html", null, true);
        echo "')\"></div>
                <div class=\"w-full py-10 px-10\">
                    <div class=\"text-center mb-10\">
                        <h1 class=\"font-bold text-3xl text-gray-900\">THOLDI</h1>
                        <p class=\"font-medium\">Veuillez vous inscrire</p>
                    </div>
                    <div>
                        <form name=\"connexion\" method=\"POST\" action=\"\" id=\"form_connexion\">
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Email</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-email-outline text-gray-400 text-lg\"></i></div>
                                        <input name=\"email\" id=\"email\" type=\"email\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new RuntimeError('Variable "last_username" does not exist.', 20, $this->source); })()), "html", null, true);
        echo "\" autocomplete=\"email\" required class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"tholdien@exemple.com\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-12\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Mot de passe</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-lock-outline text-gray-400 text-lg\"></i></div>
                                        <input name=\"password\" id=\"password\" type=\"password\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"************\" required>
                                    </div>
                                </div>
                            </div>
                            <input type=\"hidden\" name=\"_csrf_token\"
                                   value=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Bridge\Twig\Extension\CsrfRuntime')->getCsrfToken("authenticate"), "html", null, true);
        echo "\"
                            >
                            <div class=\"-mx-3\">
                                <div class=\"w-full h-24 px-3 mb-5 text-center\">
                                    <button name=\"submit\" type=\"submit\" class=\"block w-full max-w-xs mx-auto bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 duration-500 text-white rounded-lg px-3 py-3 font-semibold\">SE CONNECTER</button>
                                    <div class=\"mt-5\">
                                        <a class=\"font-medium hover:text-blue-500 duration-300\" href=\"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_register");
        echo "\">Vous n'avez pas de compte ?</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "front/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 40,  106 => 34,  89 => 20,  73 => 7,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"animate-in slide-in-from-right duration-700 absolute w-full top-52 items-center justify-center\">
        <div class=\"mx-auto my-auto bg-gray-100 text-gray-500 rounded-3xl shadow-xl overflow-hidden\" style=\"max-width:1000px\">
            <div class=\"md:flex w-full\">
                <div class=\"md:block w-1/2 py-10 px-10 bg-cover\" style=\"background-image: url('{{ asset('build/static/img/connexion1.jpg') }}')\"></div>
                <div class=\"w-full py-10 px-10\">
                    <div class=\"text-center mb-10\">
                        <h1 class=\"font-bold text-3xl text-gray-900\">THOLDI</h1>
                        <p class=\"font-medium\">Veuillez vous inscrire</p>
                    </div>
                    <div>
                        <form name=\"connexion\" method=\"POST\" action=\"\" id=\"form_connexion\">
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Email</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-email-outline text-gray-400 text-lg\"></i></div>
                                        <input name=\"email\" id=\"email\" type=\"email\" value=\"{{ last_username }}\" autocomplete=\"email\" required class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"tholdien@exemple.com\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-12\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Mot de passe</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-lock-outline text-gray-400 text-lg\"></i></div>
                                        <input name=\"password\" id=\"password\" type=\"password\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"************\" required>
                                    </div>
                                </div>
                            </div>
                            <input type=\"hidden\" name=\"_csrf_token\"
                                   value=\"{{ csrf_token('authenticate') }}\"
                            >
                            <div class=\"-mx-3\">
                                <div class=\"w-full h-24 px-3 mb-5 text-center\">
                                    <button name=\"submit\" type=\"submit\" class=\"block w-full max-w-xs mx-auto bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 duration-500 text-white rounded-lg px-3 py-3 font-semibold\">SE CONNECTER</button>
                                    <div class=\"mt-5\">
                                        <a class=\"font-medium hover:text-blue-500 duration-300\" href=\"{{ path('app_register') }}\">Vous n'avez pas de compte ?</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
{% endblock %}

", "front/login.html.twig", "/var/www/tholdiv2/templates/front/login.html.twig");
    }
}
