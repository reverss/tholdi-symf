<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/stockage.html.twig */
class __TwigTemplate_7b8a0aa955688cc644f05bbed612c56acad2012622767b93bb8a36310a71d3da extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/stockage.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/stockage.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "front/stockage.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"h-screen w-4/5 mt-24\">
        <div class=\"animate-in slide-in-from-right duration-700 py-6\">
            <div class=\"mx-48 w-full h-screen grid gap-28 grid-rows-3 grid-cols-3\">
                ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ContainersList"]) || array_key_exists("ContainersList", $context) ? $context["ContainersList"] : (function () { throw new RuntimeError('Variable "ContainersList" does not exist.', 7, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["Container"]) {
            // line 8
            echo "                    <div class=\"flex w-full h-44 bg-white drop-shadow-lg rounded-lg overflow-hidden\">
                        <div class=\" w-1/3 bg-cover bg-center object-cover\">
                            <img class=\"w-32 mt-10\" src=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(twig_get_attribute($this->env, $this->source, $context["Container"], "photo", [], "any", false, false, false, 10)), "html", null, true);
            echo "\" alt=\"\">
                        </div>
                        <div class=\"w-3/4 p-4\">
                            <h1 class=\"text-gray-900 font-bold text-2xl\">";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Container"], "libelleTypeContainer", [], "any", false, false, false, 13), "html", null, true);
            echo "</h1>
                            <p class=\"mt-2 text-gray-600 text-sm\">Volume ";
            // line 14
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Container"], "volume", [], "any", false, false, false, 14), "html", null, true);
            echo "m³</p>
                            <a href=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_container", ["slug" => twig_get_attribute($this->env, $this->source, $context["Container"], "typeContainer", [], "any", false, false, false, 15)]), "html", null, true);
            echo "\" class=\"float-right px-3 py-2 bg-gray-800 text-white text-xs font-bold uppercase rounded-lg hover:bg-blue-500 duration-500\">Voir plus</a>
                            <div class=\"item-center mt-16\">
                                <p class=\"text-xs text-gray-700\">A partir de <strong class=\"\">";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Container"], "tarifJour", [], "any", false, false, false, 17), "html", null, true);
            echo "€</strong>/jour ou <strong class=\"\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Container"], "tarifTrim", [], "any", false, false, false, 17), "html", null, true);
            echo "€</strong>/mois</p>
                            </div>
                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Container'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "            </div>
        </div>
    </main>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "front/stockage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 22,  100 => 17,  95 => 15,  91 => 14,  87 => 13,  81 => 10,  77 => 8,  73 => 7,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"h-screen w-4/5 mt-24\">
        <div class=\"animate-in slide-in-from-right duration-700 py-6\">
            <div class=\"mx-48 w-full h-screen grid gap-28 grid-rows-3 grid-cols-3\">
                {% for Container in ContainersList  %}
                    <div class=\"flex w-full h-44 bg-white drop-shadow-lg rounded-lg overflow-hidden\">
                        <div class=\" w-1/3 bg-cover bg-center object-cover\">
                            <img class=\"w-32 mt-10\" src=\"{{ asset(Container.photo) }}\" alt=\"\">
                        </div>
                        <div class=\"w-3/4 p-4\">
                            <h1 class=\"text-gray-900 font-bold text-2xl\">{{ Container.libelleTypeContainer }}</h1>
                            <p class=\"mt-2 text-gray-600 text-sm\">Volume {{ Container.volume }}m³</p>
                            <a href=\"{{ path('app_container', {'slug': Container.typeContainer}) }}\" class=\"float-right px-3 py-2 bg-gray-800 text-white text-xs font-bold uppercase rounded-lg hover:bg-blue-500 duration-500\">Voir plus</a>
                            <div class=\"item-center mt-16\">
                                <p class=\"text-xs text-gray-700\">A partir de <strong class=\"\">{{ Container.tarifJour }}€</strong>/jour ou <strong class=\"\">{{ Container.tarifTrim }}€</strong>/mois</p>
                            </div>
                        </div>
                    </div>
                {% endfor %}
            </div>
        </div>
    </main>
{% endblock %}

", "front/stockage.html.twig", "/var/www/tholdiv2/templates/front/stockage.html.twig");
    }
}
