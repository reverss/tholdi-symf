<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/reservation.html.twig */
class __TwigTemplate_5018af9e0b890a9aac795be2a22ab5a34c89396b0b93e8e42ec849ffcf7a517c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/reservation.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/reservation.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "front/reservation.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"w-2/5 h-3/6 mx-auto mt-32 border-2 rounded-2xl pt-6\">
        <form class=\"w-5/6 mx-auto mt-12\" method=\"POST\" action=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("internal_reservationadd");
        echo "\">
            <div date-rangepicker class=\"flex items-center\">
                <div class=\"relative\">
                    <div class=\"flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none\">
                        <svg aria-hidden=\"true\" class=\"w-5 h-5 text-gray-500 dark:text-gray-400\" fill=\"currentColor\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z\" clip-rule=\"evenodd\"></path></svg>
                    </div>
                    <input autocomplete=\"off\" datepicker datepicker-format=\"dd/mm/yyyy\" name=\"dateDebutReservation\" type=\"text\" class=\"bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500\" placeholder=\"Selectionnez une date\">
                </div>
                <span class=\"mx-4 text-gray-500\">au</span>
                <div class=\"relative\">
                    <div class=\"flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none\">
                        <svg aria-hidden=\"true\" class=\"w-5 h-5 text-gray-500 dark:text-gray-400\" fill=\"currentColor\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z\" clip-rule=\"evenodd\"></path></svg>
                    </div>
                    <input autocomplete=\"off\" datepicker datepicker-format=\"dd/mm/yyyy\" name=\"dateFinReservation\" type=\"text\" class=\"bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500\" placeholder=\"Selectionnez une date\">
                </div>
            </div>
            <div class=\"flex -mx-3 mt-4\">
                <div class=\"w-full px-3 mb-5\">
                    <label for=\"\" class=\"text-xs font-semibold px-1\">Volume estimé</label>
                    <div class=\"flex\">
                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-email-outline text-gray-400 text-lg\"></i></div>
                        <input name=\"volumeEstime\" type=\"int\" class=\"w-1/6 -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 placeholder:text-black/20 focus:placeholder:text-white\" placeholder=\"20\">
                    </div>
                </div>
            </div>
            <div class=\"flex -mx-3\">
                <div class=\"w-full px-3 mb-12\">
                    <label for=\"\" class=\"text-xs font-semibold px-1\">Ville départ</label>
                    <select class=\"w-auto rounded-xl px-4 py-1 mt-6\" name=\"codeVilleMiseDispo\">
                        ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["Villes"]) || array_key_exists("Villes", $context) ? $context["Villes"] : (function () { throw new RuntimeError('Variable "Villes" does not exist.', 34, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["Ville"]) {
            // line 35
            echo "                            <option value=";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Ville"], "id", [], "any", false, false, false, 35), "html", null, true);
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Ville"], "nomVille", [], "any", false, false, false, 35), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Ville'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "                    </select>
                </div>
            </div>
            <div class=\"flex -mx-3\">
                <div class=\"w-full px-3 mb-12\">
                    <label for=\"\" class=\"text-xs font-semibold px-1\">Ville arrivée</label>
                    <select class=\"w-auto rounded-xl px-4 py-1 mt-6\" name=\"codeVilleRendre\">
                        ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["Villes"]) || array_key_exists("Villes", $context) ? $context["Villes"] : (function () { throw new RuntimeError('Variable "Villes" does not exist.', 44, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["Ville"]) {
            // line 45
            echo "                            <option value=";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Ville"], "id", [], "any", false, false, false, 45), "html", null, true);
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Ville"], "nomVille", [], "any", false, false, false, 45), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Ville'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "                    </select>
                </div>
                <div class=\"-mx-3\">
                    <div class=\"w-full h-24 px-3 text-center\">
                        <button type=\"submit\" class=\"block w-full mx-auto bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 text-white rounded-lg px-6 py-3 font-semibold\">Envoyer</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script src=\"https://unpkg.com/flowbite@1.4.5/dist/datepicker.js\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "front/reservation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 47,  131 => 45,  127 => 44,  118 => 37,  107 => 35,  103 => 34,  71 => 5,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"w-2/5 h-3/6 mx-auto mt-32 border-2 rounded-2xl pt-6\">
        <form class=\"w-5/6 mx-auto mt-12\" method=\"POST\" action=\"{{ path('internal_reservationadd') }}\">
            <div date-rangepicker class=\"flex items-center\">
                <div class=\"relative\">
                    <div class=\"flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none\">
                        <svg aria-hidden=\"true\" class=\"w-5 h-5 text-gray-500 dark:text-gray-400\" fill=\"currentColor\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z\" clip-rule=\"evenodd\"></path></svg>
                    </div>
                    <input autocomplete=\"off\" datepicker datepicker-format=\"dd/mm/yyyy\" name=\"dateDebutReservation\" type=\"text\" class=\"bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500\" placeholder=\"Selectionnez une date\">
                </div>
                <span class=\"mx-4 text-gray-500\">au</span>
                <div class=\"relative\">
                    <div class=\"flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none\">
                        <svg aria-hidden=\"true\" class=\"w-5 h-5 text-gray-500 dark:text-gray-400\" fill=\"currentColor\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z\" clip-rule=\"evenodd\"></path></svg>
                    </div>
                    <input autocomplete=\"off\" datepicker datepicker-format=\"dd/mm/yyyy\" name=\"dateFinReservation\" type=\"text\" class=\"bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500\" placeholder=\"Selectionnez une date\">
                </div>
            </div>
            <div class=\"flex -mx-3 mt-4\">
                <div class=\"w-full px-3 mb-5\">
                    <label for=\"\" class=\"text-xs font-semibold px-1\">Volume estimé</label>
                    <div class=\"flex\">
                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-email-outline text-gray-400 text-lg\"></i></div>
                        <input name=\"volumeEstime\" type=\"int\" class=\"w-1/6 -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 placeholder:text-black/20 focus:placeholder:text-white\" placeholder=\"20\">
                    </div>
                </div>
            </div>
            <div class=\"flex -mx-3\">
                <div class=\"w-full px-3 mb-12\">
                    <label for=\"\" class=\"text-xs font-semibold px-1\">Ville départ</label>
                    <select class=\"w-auto rounded-xl px-4 py-1 mt-6\" name=\"codeVilleMiseDispo\">
                        {% for Ville in Villes %}
                            <option value={{ Ville.id }}>{{ Ville.nomVille }}</option>
                        {% endfor %}
                    </select>
                </div>
            </div>
            <div class=\"flex -mx-3\">
                <div class=\"w-full px-3 mb-12\">
                    <label for=\"\" class=\"text-xs font-semibold px-1\">Ville arrivée</label>
                    <select class=\"w-auto rounded-xl px-4 py-1 mt-6\" name=\"codeVilleRendre\">
                        {% for Ville in Villes %}
                            <option value={{ Ville.id }}>{{ Ville.nomVille }}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class=\"-mx-3\">
                    <div class=\"w-full h-24 px-3 text-center\">
                        <button type=\"submit\" class=\"block w-full mx-auto bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 text-white rounded-lg px-6 py-3 font-semibold\">Envoyer</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script src=\"https://unpkg.com/flowbite@1.4.5/dist/datepicker.js\"></script>
{% endblock %}

", "front/reservation.html.twig", "/var/www/tholdiv2/templates/front/reservation.html.twig");
    }
}
