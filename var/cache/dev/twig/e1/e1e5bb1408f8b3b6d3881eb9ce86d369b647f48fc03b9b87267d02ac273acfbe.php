<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_d1719dd380c3f65f25892b309b6d68486f3b1a386b3edc630531d6490ff526ba extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.4/flowbite.min.css\" rel=\"stylesheet\"/>
    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/6.6.6/css/flag-icons.min.css\" rel=\"stylesheet\" />
    <link rel=\"icon\" href=\"data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>\">
    ";
        // line 10
        echo "    ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 13
        echo "
    ";
        // line 14
        $this->displayBlock('javascripts', $context, $blocks);
        // line 17
        echo "</head>
<body class=\"overflow-y-hidden overflow-x-hidden h-screen font-Poppins\">
<nav class=\"w-4/5 p-5 mx-auto flex flex-row items-center justify-between lg:items-center\">
    <div class=\"flex justify-between\">
        <div>
            <a class=\"text-[30px] font-bold text-black hover:text-blue-500 duration-300 lg:text-3xl\" href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
        echo "\">THOLDI</a>
        </div>
        ";
        // line 36
        echo "    </div>
    <div class=\"flex gap-x-20 font-medium\">
        <a class=\"text-black lg:px-6 dark:hover:text-blue-400 hover:text-blue-400 transtion duration-300\" href=\"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_stockage");
        echo "\">Nos stockages</a>
        <a class=\"text-black lg:px-6 dark:hover:text-blue-400 hover:text-blue-400 transtion duration-300\" href=\"\">Qui sommes-nous ?</a>
        <a class=\"text-black lg:px-6 dark:hover:text-blue-400 hover:text-blue-400 transtion duration-300\" href=\"\">Nous contacter</a>
    </div>
    ";
        // line 42
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 42, $this->source); })()), "user", [], "any", false, false, false, 42) != null)) {
            // line 43
            echo "    <div class=\"\" x-data=\"{ isOpen: false}\">
        <button @click=\"isOpen = !isOpen\"
                @keydown.escape=\"isOpen = false\"
                class=\"flex items-center hover:text-blue-500 duration-300\">
            <div class=\"relative my-auto\">
                <img src=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/static/img/imageProfil.png"), "html", null, true);
            echo "\" class=\" w-10 h-10 text-black mr-5  rounded-full\" alt=\"\">
                <span class=\"top-0 left-7 absolute  w-3.5 h-3.5 bg-green-400 border-2 border-white dark:border-gray-800 rounded-full\"></span>
            </div>
            <span class=\"font-medium\">";
            // line 51
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 51, $this->source); })()), "user", [], "any", false, false, false, 51), "login", [], "any", false, false, false, 51)), "html", null, true);
            echo "<i class=\"ml-2 text-sm fas fa-chevron-down duration-300\" :class=\"{'rotate-0': !isOpen, 'rotate-180': isOpen}\"></i></span>
        </button>
        <ul x-show=\"isOpen\"
            @click.away=\"isOpen = false\"
            class=\"absolute font-normal bg-white shadow overflow-hidden rounded-xl w-48 border py-1 top-0 right-0 mt-14 mr-10 z-20\">
            <li>
                <div class=\"relative w-10 my-auto mx-auto pb-2 pt-2\">
                    <img src=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/static/img/imageProfil.png"), "html", null, true);
            echo "\" class=\" w-10 h-10 text-black mr-5 rounded-full\" alt=\"\">
                    <span class=\"top-2 left-7 absolute w-3.5 h-3.5 bg-green-400 border-2 border-white dark:border-gray-800 rounded-full\"></span>
                </div>
            </li>
            <li class=\"border-t-2 \">
            <li class=\"border-b-2\">
                <a href=\"";
            // line 64
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_profile");
            echo "\" class=\"flex items-center px-3 py-3 hover:bg-blue-100\">
                    <i class=\"fa-solid fa-user\"></i>
                    <span class=\"font-medium ml-3\">|</span>
                    <span class=\"font-medium ml-3\">Mon Profil</span>
                </a>
            </li>
            <li>
                <a href=\"";
            // line 71
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\" class=\"flex items-center px-3 py-3 hover:bg-blue-100\">
                    <i class=\"fa-solid fa-right-from-bracket\"></i>
                    <span class=\"font-medium ml-3\">|</span>
                    <span class=\"font-medium ml-3\">Déconnexion</span>
                </a>
            </li>
        </ul>
    </div>
    ";
        } else {
            // line 80
            echo "    <button type=\"button\">
        <a class=\"block h-10 px-5 py-2 font-semibold text-sm tracking-[0.5px] text-center text-black  hover:bg-gray-100 transition duration-200 border-2 rounded-lg border-gray-300 hover:border-neutral-600 hover:text-blue-500 lg:w-auto\" href=\"";
            // line 81
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
            echo "\">Connexion</a>
    </button>
    ";
        }
        // line 84
        echo "
    <!--<button type=\"button\">
        <a class=\"block h-10 px-5 py-2 font-semibold text-sm text-center text-black lg:mt-0 hover:bg-gray-100 transition duration-200 border-2 rounded-lg border-gray-300 hover:border-neutral-600 hover:text-blue-500 lg:w-auto\" href=\"connexion.php\">Connexion</a>
    </button>-->
</nav>
";
        // line 89
        $this->displayBlock('body', $context, $blocks);
        // line 90
        echo "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.4/flowbite.min.js\"></script>
<script defer src=\"https://unpkg.com/alpinejs@3.10.5/dist/cdn.min.js\"></script>
<script src=\"https://kit.fontawesome.com/b8c6b83e3e.js\" crossorigin=\"anonymous\"></script>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Tholdi - Réservation de conteneur de stockage";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "        ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("app");
        echo "
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 15
        echo "        ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app");
        echo "
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 89
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 89,  232 => 15,  222 => 14,  209 => 11,  199 => 10,  180 => 5,  165 => 90,  163 => 89,  156 => 84,  150 => 81,  147 => 80,  135 => 71,  125 => 64,  116 => 58,  106 => 51,  100 => 48,  93 => 43,  91 => 42,  84 => 38,  80 => 36,  75 => 22,  68 => 17,  66 => 14,  63 => 13,  60 => 10,  53 => 5,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"fr\">
<head>
    <meta charset=\"UTF-8\">
    <title>{% block title %}Tholdi - Réservation de conteneur de stockage{% endblock %}</title>
    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.4/flowbite.min.css\" rel=\"stylesheet\"/>
    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/6.6.6/css/flag-icons.min.css\" rel=\"stylesheet\" />
    <link rel=\"icon\" href=\"data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>\">
    {# Run `composer require symfony/webpack-encore-bundle` to start using Symfony UX #}
    {% block stylesheets %}
        {{ encore_entry_link_tags('app') }}
    {% endblock %}

    {% block javascripts %}
        {{ encore_entry_script_tags('app') }}
    {% endblock %}
</head>
<body class=\"overflow-y-hidden overflow-x-hidden h-screen font-Poppins\">
<nav class=\"w-4/5 p-5 mx-auto flex flex-row items-center justify-between lg:items-center\">
    <div class=\"flex justify-between\">
        <div>
            <a class=\"text-[30px] font-bold text-black hover:text-blue-500 duration-300 lg:text-3xl\" href=\"{{ path('app_home') }}\">THOLDI</a>
        </div>
        {#<!-- Mobile menu button -->
                <div class=\"flex lg:hidden\">
                    <button type=\"button\"
                            class=\"text-black focus:outline-none focus:text-gray-600\"
                            aria-label=\"toggle menu\">
                        <svg viewBox=\"0 0 24 24\" class=\"w-6 h-6 fill-current\">
                            <path fill-rule=\"evenodd\"
                                  d=\"M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z\">
                            </path>
                        </svg>
                    </button>
                </div>#}
    </div>
    <div class=\"flex gap-x-20 font-medium\">
        <a class=\"text-black lg:px-6 dark:hover:text-blue-400 hover:text-blue-400 transtion duration-300\" href=\"{{ path('app_stockage') }}\">Nos stockages</a>
        <a class=\"text-black lg:px-6 dark:hover:text-blue-400 hover:text-blue-400 transtion duration-300\" href=\"\">Qui sommes-nous ?</a>
        <a class=\"text-black lg:px-6 dark:hover:text-blue-400 hover:text-blue-400 transtion duration-300\" href=\"\">Nous contacter</a>
    </div>
    {% if app.user != null %}
    <div class=\"\" x-data=\"{ isOpen: false}\">
        <button @click=\"isOpen = !isOpen\"
                @keydown.escape=\"isOpen = false\"
                class=\"flex items-center hover:text-blue-500 duration-300\">
            <div class=\"relative my-auto\">
                <img src=\"{{ asset('build/static/img/imageProfil.png') }}\" class=\" w-10 h-10 text-black mr-5  rounded-full\" alt=\"\">
                <span class=\"top-0 left-7 absolute  w-3.5 h-3.5 bg-green-400 border-2 border-white dark:border-gray-800 rounded-full\"></span>
            </div>
            <span class=\"font-medium\">{{ app.user.login|upper }}<i class=\"ml-2 text-sm fas fa-chevron-down duration-300\" :class=\"{'rotate-0': !isOpen, 'rotate-180': isOpen}\"></i></span>
        </button>
        <ul x-show=\"isOpen\"
            @click.away=\"isOpen = false\"
            class=\"absolute font-normal bg-white shadow overflow-hidden rounded-xl w-48 border py-1 top-0 right-0 mt-14 mr-10 z-20\">
            <li>
                <div class=\"relative w-10 my-auto mx-auto pb-2 pt-2\">
                    <img src=\"{{ asset('build/static/img/imageProfil.png') }}\" class=\" w-10 h-10 text-black mr-5 rounded-full\" alt=\"\">
                    <span class=\"top-2 left-7 absolute w-3.5 h-3.5 bg-green-400 border-2 border-white dark:border-gray-800 rounded-full\"></span>
                </div>
            </li>
            <li class=\"border-t-2 \">
            <li class=\"border-b-2\">
                <a href=\"{{ path('app_profile') }}\" class=\"flex items-center px-3 py-3 hover:bg-blue-100\">
                    <i class=\"fa-solid fa-user\"></i>
                    <span class=\"font-medium ml-3\">|</span>
                    <span class=\"font-medium ml-3\">Mon Profil</span>
                </a>
            </li>
            <li>
                <a href=\"{{ path('app_logout') }}\" class=\"flex items-center px-3 py-3 hover:bg-blue-100\">
                    <i class=\"fa-solid fa-right-from-bracket\"></i>
                    <span class=\"font-medium ml-3\">|</span>
                    <span class=\"font-medium ml-3\">Déconnexion</span>
                </a>
            </li>
        </ul>
    </div>
    {% else %}
    <button type=\"button\">
        <a class=\"block h-10 px-5 py-2 font-semibold text-sm tracking-[0.5px] text-center text-black  hover:bg-gray-100 transition duration-200 border-2 rounded-lg border-gray-300 hover:border-neutral-600 hover:text-blue-500 lg:w-auto\" href=\"{{ path('app_login') }}\">Connexion</a>
    </button>
    {% endif %}

    <!--<button type=\"button\">
        <a class=\"block h-10 px-5 py-2 font-semibold text-sm text-center text-black lg:mt-0 hover:bg-gray-100 transition duration-200 border-2 rounded-lg border-gray-300 hover:border-neutral-600 hover:text-blue-500 lg:w-auto\" href=\"connexion.php\">Connexion</a>
    </button>-->
</nav>
{% block body %}{% endblock %}
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.4/flowbite.min.js\"></script>
<script defer src=\"https://unpkg.com/alpinejs@3.10.5/dist/cdn.min.js\"></script>
<script src=\"https://kit.fontawesome.com/b8c6b83e3e.js\" crossorigin=\"anonymous\"></script>
</body>
</html>
", "base.html.twig", "/var/www/tholdiv2/templates/base.html.twig");
    }
}
