<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/profil.html.twig */
class __TwigTemplate_c6b802a650e28974f703ee3540299cc5e0e9d111850054af891296d5af05a4e2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/profil.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "front/profil.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        // line 11
        echo "    <main class=\"w-full h-full pl-8\">
        <section class=\"mx-auto w-4/5 h-4/5 m-10 bg-gray-300/30 rounded-3xl p-8 border-2 border-gray-400/10\">
            <div class=\"w-full h-full\">
                <div class=\"overflow-y-auto scrollbar-thin scrollbar-thumb-transparent scrollbar-track-transparent bg-white w-3/6 h-3/6 border-2 mx-auto mb-5 drop-shadow-2xl rounded-2xl\">
                    ";
        // line 15
        if (( !array_key_exists("Reservations", $context) || twig_test_empty((isset($context["Reservations"]) || array_key_exists("Reservations", $context) ? $context["Reservations"] : (function () { throw new RuntimeError('Variable "Reservations" does not exist.', 15, $this->source); })())))) {
            // line 16
            echo "                        <div class=\"w-3/6 mx-auto mt-5 rounded-2xl\">
                            <img class=\"w-full h-3/5\" src=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/static/img/emptyReservation.png"), "html", null, true);
            echo "\" alt=\"\">
                        </div>
                    ";
        } else {
            // line 20
            echo "                        <h1 class=\"w-auto text-center mx-auto mt-5 text-2xl font-bold\">Mes réservations</h1>
                        ";
            // line 21
            if (array_key_exists("Reservations", $context)) {
                // line 22
                echo "                            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["Reservations"]) || array_key_exists("Reservations", $context) ? $context["Reservations"] : (function () { throw new RuntimeError('Variable "Reservations" does not exist.', 22, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["Reservation"]) {
                    // line 23
                    echo "                                <div class=\"w-full flex\">
                                    <div class=\"w-3/5 p-4 ml-14 mt-3\">
                                        <span class=\"absolute left-4 ml-6 mt-0.5 bg-orange-400 border-2 font-medium text-sm rounded-full px-3.5 py-2\">";
                    // line 25
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Reservation"], "etat", [], "any", false, false, false, 25), "html", null, true);
                    echo "</span>
                                        <h2 class=\"border-l-2 pl-4 ml-8\">Commande n°";
                    // line 26
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Reservation"], "id", [], "any", false, false, false, 26), "html", null, true);
                    echo "</h2>
                                        <h2 class=\"border-l-2 text-sm pl-4 ml-8\">Reservé le: ";
                    // line 27
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Reservation"], "dateReservation", [], "any", false, false, false, 27), "d/m/Y"), "html", null, true);
                    echo "</h2>
                                    </div>
                                    <div class=\"w-full\">
                                        <button type=\"submit\" value=\"\" class=\"float-right text-sm font-medium mt-8 mr-12 p-2 px-3 hover:text-blue-500 transform-cpu duration-200\">Voir Info</button>
                                    </div>
                                </div>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Reservation'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 34
                echo "                        ";
            }
            // line 35
            echo "                    ";
        }
        // line 36
        echo "                </div>
                <div class=\"overflow-y-auto scrollbar-thin scrollbar-thumb-transparent scrollbar-track-transparent border-2 bg-white w-3/6 h-3/6 mx-auto drop-shadow-2xl rounded-2xl\">
                    ";
        // line 38
        if (( !array_key_exists("Paniers", $context) || twig_test_empty((isset($context["Paniers"]) || array_key_exists("Paniers", $context) ? $context["Paniers"] : (function () { throw new RuntimeError('Variable "Paniers" does not exist.', 38, $this->source); })())))) {
            // line 39
            echo "                        <div class=\"w-3/6 mx-auto mt-10 rounded-2xl\">
                            <img src=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/static/img/emptyCart.png"), "html", null, true);
            echo "\" alt=\"\">
                        </div>
                    ";
        } else {
            // line 43
            echo "                    <h1 class=\"text-center mx-auto mt-5 text-2xl font-bold\">MON PANIER</h1>
                    ";
            // line 44
            if (array_key_exists("Paniers", $context)) {
                // line 45
                echo "                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["Paniers"]) || array_key_exists("Paniers", $context) ? $context["Paniers"] : (function () { throw new RuntimeError('Variable "Paniers" does not exist.', 45, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["Panier"]) {
                    // line 46
                    echo "                            <div class=\"flex\">
                                <div class=\"w-2/5 p-3 ml-16 mt-3\">
                                    <img class=\"absolute left-4 ml-8 mt-0.5 w-10 h-10 object-cover border-2 rounded-full\" src=\"";
                    // line 48
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["Panier"], "typeContainer", [], "any", false, false, false, 48), "photo", [], "any", false, false, false, 48), "html", null, true);
                    echo "\" alt=\"\">
                                    <h2 class=\"border-l-2 pl-4 text-xl font-medium ml-6\">";
                    // line 49
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["Panier"], "typeContainer", [], "any", false, false, false, 49), "libelleTypeContainer", [], "any", false, false, false, 49), "html", null, true);
                    echo "</h2>
                                    <h2 class=\"border-l-2 pl-4 ml-6\">Mode de paiement: </h2>
                                    <h2 class=\"border-l-2 text-sm pl-4 ml-6\">Quantité: ";
                    // line 51
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["Panier"], "quantite", [], "any", false, false, false, 51), "html", null, true);
                    echo "</h2>
                                </div>
                                <div class=\"w-full\">
                                    <a href=\"";
                    // line 54
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("internal_cart_cartdelete", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["Panier"], "typeContainer", [], "any", false, false, false, 54), "typeContainer", [], "any", false, false, false, 54)]), "html", null, true);
                    echo "\" id=\"suppression\" type=\"submit\"  class=\"float-right text-sm font-medium mt-11 mr-12 rounded-full p-2 hover:text-red-500 duration-500\">Supprimer</a>
                                </div>
                            </div>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Panier'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 58
                echo "                    ";
            }
            // line 59
            echo "                        <div class=\"w-full text-center\">
                            <button class=\"mt-3 mb-4 p-2 font-medium hover:text-green-600 duration-500\"><a href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_reservation", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 60, $this->source); })()), "user", [], "any", false, false, false, 60), "id", [], "any", false, false, false, 60)]), "html", null, true);
            echo "\">Valider panier</a></button>
                        </div>
                ";
        }
        // line 63
        echo "            </div>
            </div>
        </section>
    </main>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "front/profil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 63,  179 => 60,  176 => 59,  173 => 58,  163 => 54,  157 => 51,  152 => 49,  148 => 48,  144 => 46,  139 => 45,  137 => 44,  134 => 43,  128 => 40,  125 => 39,  123 => 38,  119 => 36,  116 => 35,  113 => 34,  100 => 27,  96 => 26,  92 => 25,  88 => 23,  83 => 22,  81 => 21,  78 => 20,  72 => 17,  69 => 16,  67 => 15,  61 => 11,  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    {#\$nbArticles = 0;

    \$reservation = obtenirDetailReservationParCodeClient(\$_SESSION['codeClient']);

    if (isset(\$reservation['codeReservation'])) {

    \$reserver = obtenirDetailReserver(\$reservation['codeReservation']);#}
    <main class=\"w-full h-full pl-8\">
        <section class=\"mx-auto w-4/5 h-4/5 m-10 bg-gray-300/30 rounded-3xl p-8 border-2 border-gray-400/10\">
            <div class=\"w-full h-full\">
                <div class=\"overflow-y-auto scrollbar-thin scrollbar-thumb-transparent scrollbar-track-transparent bg-white w-3/6 h-3/6 border-2 mx-auto mb-5 drop-shadow-2xl rounded-2xl\">
                    {% if Reservations is not defined or Reservations is empty %}
                        <div class=\"w-3/6 mx-auto mt-5 rounded-2xl\">
                            <img class=\"w-full h-3/5\" src=\"{{ asset('build/static/img/emptyReservation.png') }}\" alt=\"\">
                        </div>
                    {% else %}
                        <h1 class=\"w-auto text-center mx-auto mt-5 text-2xl font-bold\">Mes réservations</h1>
                        {% if Reservations is defined %}
                            {% for Reservation in Reservations %}
                                <div class=\"w-full flex\">
                                    <div class=\"w-3/5 p-4 ml-14 mt-3\">
                                        <span class=\"absolute left-4 ml-6 mt-0.5 bg-orange-400 border-2 font-medium text-sm rounded-full px-3.5 py-2\">{{ Reservation.etat }}</span>
                                        <h2 class=\"border-l-2 pl-4 ml-8\">Commande n°{{ Reservation.id }}</h2>
                                        <h2 class=\"border-l-2 text-sm pl-4 ml-8\">Reservé le: {{ Reservation.dateReservation|date('d/m/Y') }}</h2>
                                    </div>
                                    <div class=\"w-full\">
                                        <button type=\"submit\" value=\"\" class=\"float-right text-sm font-medium mt-8 mr-12 p-2 px-3 hover:text-blue-500 transform-cpu duration-200\">Voir Info</button>
                                    </div>
                                </div>
                            {% endfor %}
                        {% endif %}
                    {% endif %}
                </div>
                <div class=\"overflow-y-auto scrollbar-thin scrollbar-thumb-transparent scrollbar-track-transparent border-2 bg-white w-3/6 h-3/6 mx-auto drop-shadow-2xl rounded-2xl\">
                    {% if Paniers is not defined or Paniers is empty %}
                        <div class=\"w-3/6 mx-auto mt-10 rounded-2xl\">
                            <img src=\"{{ asset('build/static/img/emptyCart.png') }}\" alt=\"\">
                        </div>
                    {% else %}
                    <h1 class=\"text-center mx-auto mt-5 text-2xl font-bold\">MON PANIER</h1>
                    {% if Paniers is defined %}
                        {% for Panier in Paniers %}
                            <div class=\"flex\">
                                <div class=\"w-2/5 p-3 ml-16 mt-3\">
                                    <img class=\"absolute left-4 ml-8 mt-0.5 w-10 h-10 object-cover border-2 rounded-full\" src=\"{{ Panier.typeContainer.photo }}\" alt=\"\">
                                    <h2 class=\"border-l-2 pl-4 text-xl font-medium ml-6\">{{ Panier.typeContainer.libelleTypeContainer }}</h2>
                                    <h2 class=\"border-l-2 pl-4 ml-6\">Mode de paiement: </h2>
                                    <h2 class=\"border-l-2 text-sm pl-4 ml-6\">Quantité: {{ Panier.quantite }}</h2>
                                </div>
                                <div class=\"w-full\">
                                    <a href=\"{{ path('internal_cart_cartdelete', {'slug': Panier.typeContainer.typeContainer}) }}\" id=\"suppression\" type=\"submit\"  class=\"float-right text-sm font-medium mt-11 mr-12 rounded-full p-2 hover:text-red-500 duration-500\">Supprimer</a>
                                </div>
                            </div>
                        {% endfor %}
                    {% endif %}
                        <div class=\"w-full text-center\">
                            <button class=\"mt-3 mb-4 p-2 font-medium hover:text-green-600 duration-500\"><a href=\"{{ path('app_reservation', {'slug': app.user.id }) }}\">Valider panier</a></button>
                        </div>
                {% endif %}
            </div>
            </div>
        </section>
    </main>
{% endblock %}

", "front/profil.html.twig", "/var/www/tholdiv2/templates/front/profil.html.twig");
    }
}
