<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/conteneur.html.twig */
class __TwigTemplate_3cf9159d6067d1641ebf25c603d995722af48c01d28303ccfbd3dccb7b968971 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/conteneur.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "front/conteneur.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"h-full w-4/5 mt-14 mx-auto\">
        <div class=\"w-full h-10\">
            <a class=\"font-medium text-sm hover:text-blue-400 duration-200\" href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_stockage");
        echo "\"><span class=\"text-xs font-light pr-2\"><i class=\"bg-blue-100 fa-solid fa-angle-left border px-3 py-2 rounded-full text-blue-400\"></i></span>Tous les conteneurs</a>
            <ul class=\"float-right text-sm font-medium\">
                <li class=\"inline-block text-gray-500\"><a class=\"hover:text-blue-400 duration-300\" href=\"";
        // line 8
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
        echo "\">Accueil</a></li>
                <li class=\"ml-2 fa-solid fa-angle-right text-xs text-gray-500\"></li>
                <li class=\"ml-2 inline-block text-gray-500\"><a class=\"hover:text-blue-400 duration-300\" href=\"";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_stockage");
        echo "\">Nos stockages</a></li>
                <i class=\"ml-2 fa-solid fa-angle-right text-xs text-gray-500\"></i>
                <li class=\"ml-2 inline-block\">Conteneur <?php echo \$unContainer['libelleTypeContainer']?></li>
            </ul>
        </div>
        <div class=\"w-full h-px border rounded-xl mt-3\"></div>
        <sectionL class=\"animate-in slide-in-from-left duration-700 w-2/5 float-left mt-20 ml-24\">
            <div id=\"animation-carousel\" class=\"relative\" data-carousel=\"static\">
                <!-- Création du caroussel -->
                <div class=\"overflow-hidden relative rounded-lg h-96\">
                    <!-- Element 1 -->
                    <div class=\"hidden duration-200 ease-linear\" data-carousel-item=\"active\">
                        <img src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(twig_get_attribute($this->env, $this->source, (isset($context["Container"]) || array_key_exists("Container", $context) ? $context["Container"] : (function () { throw new RuntimeError('Variable "Container" does not exist.', 22, $this->source); })()), "photo", [], "any", false, false, false, 22)), "html", null, true);
        echo "\" class=\"block absolute top-1/2 left-1/2 w-full -translate-x-1/2 -translate-y-1/2\" alt=\"...\">
                    </div>
                    <!-- Element 2 -->
                    <div class=\"hidden duration-200 ease-linear\" data-carousel-item>
                        <img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(twig_get_attribute($this->env, $this->source, (isset($context["Container"]) || array_key_exists("Container", $context) ? $context["Container"] : (function () { throw new RuntimeError('Variable "Container" does not exist.', 26, $this->source); })()), "photo2", [], "any", false, false, false, 26)), "html", null, true);
        echo "\" class=\"block absolute top-1/2 left-1/2 w-full -translate-x-1/2 -translate-y-1/2\" alt=\"...\">
                    </div>
                    <!-- Element 3 -->
                    <div class=\"hidden duration-200 ease-linear\" data-carousel-item>
                        <img src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(twig_get_attribute($this->env, $this->source, (isset($context["Container"]) || array_key_exists("Container", $context) ? $context["Container"] : (function () { throw new RuntimeError('Variable "Container" does not exist.', 30, $this->source); })()), "photo3", [], "any", false, false, false, 30)), "html", null, true);
        echo "\" class=\"block absolute top-1/2 left-1/2 w-full -translate-x-1/2 -translate-y-1/2\" alt=\"...\">
                    </div>
                    <!-- Element 4 -->
                    <div class=\"hidden duration-200 ease-linear\" data-carousel-item>
                        <img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(twig_get_attribute($this->env, $this->source, (isset($context["Container"]) || array_key_exists("Container", $context) ? $context["Container"] : (function () { throw new RuntimeError('Variable "Container" does not exist.', 34, $this->source); })()), "photo4", [], "any", false, false, false, 34)), "html", null, true);
        echo "\" class=\"block absolute top-1/2 left-1/2 w-full -translate-x-1/2 -translate-y-1/2\" alt=\"...\">
                    </div>
                </div>
                <!-- Bouton / Slider -->
                <button type=\"button\" class=\"flex absolute top-0 left-0 z-30 justify-center items-center px-4 h-full cursor-pointer group focus:outline-none\" data-carousel-prev>
\t\t\t\t<span class=\"inline-flex justify-center items-center w-8 h-8 rounded-full sm:w-10 sm:h-10 group-focus:outline-none\">
\t\t\t\t\t<svg class=\"w-5 h-5 text-black sm:w-6 sm:h-6 dark:text-gray-800\" fill=\"none\" stroke=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\"><path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M15 19l-7-7 7-7\"></path></svg>
\t\t\t\t\t<span class=\"hidden\">Previous</span>
\t\t\t\t</span>
                </button>
                <button type=\"button\" class=\"flex absolute top-0 right-0 z-30 justify-center items-center px-4 h-full cursor-pointer group focus:outline-none\" data-carousel-next>
\t\t\t\t<span class=\"inline-flex justify-center items-center w-8 h-8 rounded-full sm:w-10 sm:h-10 group-focus:outline-none\">
\t\t\t\t\t<svg class=\"w-5 h-5 text-black sm:w-6 sm:h-6 dark:text-gray-800\" fill=\"none\" stroke=\"currentColor\" viewBox=\"0 0 24 24\" xmlns=\"http://www.w3.org/2000/svg\"><path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M9 5l7 7-7 7\"></path></svg>
\t\t\t\t\t<span class=\"hidden\">Next</span>
\t\t\t\t</span>
                </button>
            </div>
        </sectionL>
        <sectionR class=\"animate-in slide-in-from-right duration-700 w-2/5 h-full mt-10 float-right\">
            <div class=\"mt-14 tracking-widest font-bold text-blue-400\">VOLUME ";
        // line 53
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["Container"]) || array_key_exists("Container", $context) ? $context["Container"] : (function () { throw new RuntimeError('Variable "Container" does not exist.', 53, $this->source); })()), "volume", [], "any", false, false, false, 53), "html", null, true);
        echo "M³</div>
            <h1 class=\"mt-5 font-bold text-4xl\">";
        // line 54
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["Container"]) || array_key_exists("Container", $context) ? $context["Container"] : (function () { throw new RuntimeError('Variable "Container" does not exist.', 54, $this->source); })()), "libelleTypeContainer", [], "any", false, false, false, 54), "html", null, true);
        echo "</h1>
            <div class=\"mt-5 \">Un conteneur fait pour vous (Neuf)</div>
            <div class=\"flex mt-8\">
                <p class=\"flex\">
\t\t\t\t<span>
\t\t\t\t\t<span class=\" text-gray-500\">Location à partir de</span>
\t\t\t\t\t<br>
\t\t\t\t\t<strong class=\" text-2xl font-bold text-blue-400\">";
        // line 61
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["Container"]) || array_key_exists("Container", $context) ? $context["Container"] : (function () { throw new RuntimeError('Variable "Container" does not exist.', 61, $this->source); })()), "tarifJour", [], "any", false, false, false, 61), "html", null, true);
        echo "€</strong>
\t\t\t\t\t<sup>HT</sup>
\t\t\t\t\t/jour
\t\t\t\t</span>
                    <span class=\"ml-10\">ou</span>
                    <span class=\"ml-10\">
\t\t\t\t\t<span class=\"text-gray-500\">Location à partir de</span>
\t\t\t\t\t<br>
\t\t\t\t\t<strong class=\"text-2xl font-bold text-blue-400\">";
        // line 69
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["Container"]) || array_key_exists("Container", $context) ? $context["Container"] : (function () { throw new RuntimeError('Variable "Container" does not exist.', 69, $this->source); })()), "tarifTrim", [], "any", false, false, false, 69), "html", null, true);
        echo "€</strong>
\t\t\t\t\t<sup>HT</sup>
\t\t\t\t\t/mois
\t\t\t\t</span>
                </p>
            </div>
            ";
        // line 75
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 75, $this->source); })()), "user", [], "any", false, false, false, 75)) {
            // line 76
            echo "            <form method=\"POST\" action=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("internal_cart_add", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["Container"]) || array_key_exists("Container", $context) ? $context["Container"] : (function () { throw new RuntimeError('Variable "Container" does not exist.', 76, $this->source); })()), "typeContainer", [], "any", false, false, false, 76)]), "html", null, true);
            echo "\">
                <select name=\"quantite\" class=\"w-1/6 rounded-xl px-4 py-1 mt-6\">
                    <option value=\"1\">1</option>
                    <option value=\"2\">2</option>
                    <option value=\"3\">3</option>
                    <option value=\"4\">4</option>
                    <option value=\"5\">5</option>
                    <option value=\"6\">6</option>
                    <option value=\"7\">7</option>
                    <option value=\"8\">8</option>
                    <option value=\"9\">9</option>
                    <option value=\"10\">10</option>
                </select>

                <select class=\"w-auto ml-4 rounded-xl py-1 px-4\" name=\"paiement\">
                    <option value=\"Jour\">Jour</option>
                    <option value=\"Mois\">Mois</option>
                    <option value=\"Annee\">Année ";
            // line 93
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["Container"]) || array_key_exists("Container", $context) ? $context["Container"] : (function () { throw new RuntimeError('Variable "Container" does not exist.', 93, $this->source); })()), "tarifAnn", [], "any", false, false, false, 93), "html", null, true);
            echo "</option>
                </select>
                ";
        }
        // line 96
        echo "                <div class=\"mt-8\">
                    <button type=\"submit\" class=\" bg-blue-800 hover:bg-blue-500 duration-500 text-white font-bold py-3 px-32 rounded-lg\">
                        <i class=\"fa-solid fa-shopping-cart text-xl mr-2\"></i>
                        Ajouter au panier
                    </button>
                </div>
            </form>
        </sectionR>
    </main>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "front/conteneur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 96,  188 => 93,  167 => 76,  165 => 75,  156 => 69,  145 => 61,  135 => 54,  131 => 53,  109 => 34,  102 => 30,  95 => 26,  88 => 22,  73 => 10,  68 => 8,  63 => 6,  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"h-full w-4/5 mt-14 mx-auto\">
        <div class=\"w-full h-10\">
            <a class=\"font-medium text-sm hover:text-blue-400 duration-200\" href=\"{{ path('app_stockage') }}\"><span class=\"text-xs font-light pr-2\"><i class=\"bg-blue-100 fa-solid fa-angle-left border px-3 py-2 rounded-full text-blue-400\"></i></span>Tous les conteneurs</a>
            <ul class=\"float-right text-sm font-medium\">
                <li class=\"inline-block text-gray-500\"><a class=\"hover:text-blue-400 duration-300\" href=\"{{ path('app_home') }}\">Accueil</a></li>
                <li class=\"ml-2 fa-solid fa-angle-right text-xs text-gray-500\"></li>
                <li class=\"ml-2 inline-block text-gray-500\"><a class=\"hover:text-blue-400 duration-300\" href=\"{{ path('app_stockage') }}\">Nos stockages</a></li>
                <i class=\"ml-2 fa-solid fa-angle-right text-xs text-gray-500\"></i>
                <li class=\"ml-2 inline-block\">Conteneur <?php echo \$unContainer['libelleTypeContainer']?></li>
            </ul>
        </div>
        <div class=\"w-full h-px border rounded-xl mt-3\"></div>
        <sectionL class=\"animate-in slide-in-from-left duration-700 w-2/5 float-left mt-20 ml-24\">
            <div id=\"animation-carousel\" class=\"relative\" data-carousel=\"static\">
                <!-- Création du caroussel -->
                <div class=\"overflow-hidden relative rounded-lg h-96\">
                    <!-- Element 1 -->
                    <div class=\"hidden duration-200 ease-linear\" data-carousel-item=\"active\">
                        <img src=\"{{ asset(Container.photo) }}\" class=\"block absolute top-1/2 left-1/2 w-full -translate-x-1/2 -translate-y-1/2\" alt=\"...\">
                    </div>
                    <!-- Element 2 -->
                    <div class=\"hidden duration-200 ease-linear\" data-carousel-item>
                        <img src=\"{{ asset(Container.photo2) }}\" class=\"block absolute top-1/2 left-1/2 w-full -translate-x-1/2 -translate-y-1/2\" alt=\"...\">
                    </div>
                    <!-- Element 3 -->
                    <div class=\"hidden duration-200 ease-linear\" data-carousel-item>
                        <img src=\"{{ asset(Container.photo3) }}\" class=\"block absolute top-1/2 left-1/2 w-full -translate-x-1/2 -translate-y-1/2\" alt=\"...\">
                    </div>
                    <!-- Element 4 -->
                    <div class=\"hidden duration-200 ease-linear\" data-carousel-item>
                        <img src=\"{{ asset(Container.photo4) }}\" class=\"block absolute top-1/2 left-1/2 w-full -translate-x-1/2 -translate-y-1/2\" alt=\"...\">
                    </div>
                </div>
                <!-- Bouton / Slider -->
                <button type=\"button\" class=\"flex absolute top-0 left-0 z-30 justify-center items-center px-4 h-full cursor-pointer group focus:outline-none\" data-carousel-prev>
\t\t\t\t<span class=\"inline-flex justify-center items-center w-8 h-8 rounded-full sm:w-10 sm:h-10 group-focus:outline-none\">
\t\t\t\t\t<svg class=\"w-5 h-5 text-black sm:w-6 sm:h-6 dark:text-gray-800\" fill=\"none\" stroke=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\"><path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M15 19l-7-7 7-7\"></path></svg>
\t\t\t\t\t<span class=\"hidden\">Previous</span>
\t\t\t\t</span>
                </button>
                <button type=\"button\" class=\"flex absolute top-0 right-0 z-30 justify-center items-center px-4 h-full cursor-pointer group focus:outline-none\" data-carousel-next>
\t\t\t\t<span class=\"inline-flex justify-center items-center w-8 h-8 rounded-full sm:w-10 sm:h-10 group-focus:outline-none\">
\t\t\t\t\t<svg class=\"w-5 h-5 text-black sm:w-6 sm:h-6 dark:text-gray-800\" fill=\"none\" stroke=\"currentColor\" viewBox=\"0 0 24 24\" xmlns=\"http://www.w3.org/2000/svg\"><path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M9 5l7 7-7 7\"></path></svg>
\t\t\t\t\t<span class=\"hidden\">Next</span>
\t\t\t\t</span>
                </button>
            </div>
        </sectionL>
        <sectionR class=\"animate-in slide-in-from-right duration-700 w-2/5 h-full mt-10 float-right\">
            <div class=\"mt-14 tracking-widest font-bold text-blue-400\">VOLUME {{ Container.volume }}M³</div>
            <h1 class=\"mt-5 font-bold text-4xl\">{{ Container.libelleTypeContainer }}</h1>
            <div class=\"mt-5 \">Un conteneur fait pour vous (Neuf)</div>
            <div class=\"flex mt-8\">
                <p class=\"flex\">
\t\t\t\t<span>
\t\t\t\t\t<span class=\" text-gray-500\">Location à partir de</span>
\t\t\t\t\t<br>
\t\t\t\t\t<strong class=\" text-2xl font-bold text-blue-400\">{{ Container.tarifJour }}€</strong>
\t\t\t\t\t<sup>HT</sup>
\t\t\t\t\t/jour
\t\t\t\t</span>
                    <span class=\"ml-10\">ou</span>
                    <span class=\"ml-10\">
\t\t\t\t\t<span class=\"text-gray-500\">Location à partir de</span>
\t\t\t\t\t<br>
\t\t\t\t\t<strong class=\"text-2xl font-bold text-blue-400\">{{ Container.tarifTrim }}€</strong>
\t\t\t\t\t<sup>HT</sup>
\t\t\t\t\t/mois
\t\t\t\t</span>
                </p>
            </div>
            {% if app.user %}
            <form method=\"POST\" action=\"{{ path('internal_cart_add', {'slug': Container.typeContainer}) }}\">
                <select name=\"quantite\" class=\"w-1/6 rounded-xl px-4 py-1 mt-6\">
                    <option value=\"1\">1</option>
                    <option value=\"2\">2</option>
                    <option value=\"3\">3</option>
                    <option value=\"4\">4</option>
                    <option value=\"5\">5</option>
                    <option value=\"6\">6</option>
                    <option value=\"7\">7</option>
                    <option value=\"8\">8</option>
                    <option value=\"9\">9</option>
                    <option value=\"10\">10</option>
                </select>

                <select class=\"w-auto ml-4 rounded-xl py-1 px-4\" name=\"paiement\">
                    <option value=\"Jour\">Jour</option>
                    <option value=\"Mois\">Mois</option>
                    <option value=\"Annee\">Année {{ Container.tarifAnn }}</option>
                </select>
                {% endif %}
                <div class=\"mt-8\">
                    <button type=\"submit\" class=\" bg-blue-800 hover:bg-blue-500 duration-500 text-white font-bold py-3 px-32 rounded-lg\">
                        <i class=\"fa-solid fa-shopping-cart text-xl mr-2\"></i>
                        Ajouter au panier
                    </button>
                </div>
            </form>
        </sectionR>
    </main>
{% endblock %}

", "front/conteneur.html.twig", "/var/www/tholdiv2/templates/front/conteneur.html.twig");
    }
}
