<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/register.html.twig */
class __TwigTemplate_7714ca6ea1185397f8f55bdc9cb5ce87ed24c867bf2e5da9d4b3285618e1cabd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/register.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "front/register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"absolute top-24 w-full items-center justify-center\">
        <div class=\"mx-auto bg-gray-100 text-gray-500 rounded-3xl shadow-xl overflow-hidden\" style=\"max-width:1200px\">
            <div class=\"md:flex w-full\">
                <div class=\"w-3/5 py-10 px-10 bg-cover\" style=\"background-image: url('";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/static/img/connexion1.jpg"), "html", null, true);
        echo "')\"></div>
                <div class=\"w-full py-10 px-10 md:px-10\">
                    <div class=\"text-center mb-10\">
                        <h1 class=\"font-bold text-3xl text-gray-900\">THOLDI</h1>
                        <p>Veuillez vous inscrire</p>
                    </div>
                    <form method=\"POST\">
                        <div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Raison sociale</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input pattern=\"[-çéèùa-zA-Z]{3,50}\" name=\"raisonSociale\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"Tholdi\" required>
                                    </div>
                                </div>
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Pseudo</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input pattern=\"[éèa-zA-Z0-9]{3,10}\" name=\"pseudo\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"Tholdien\" required>
                                    </div>
                                </div>
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"pr-2 text-xs font-semibold px-1\">Téléphone</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input pattern=\"[0]{1}[0-9]{9}\" maxlength=\"10\" name=\"telephone\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"0102030405\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Contact</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input name=\"contact\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"Ahri Dupont\">
                                    </div>
                                </div>
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Ville</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input name=\"ville\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"Meulan\" required>
                                    </div>
                                </div>
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"pr-2 text-xs font-semibold px-1\">Code Postal</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input pattern=\"[0-9]{5}\" maxlength=\"5\" name=\"cp\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"78000\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Adresse</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-email-outline text-gray-400 text-lg\"></i></div>
                                        <input name=\"adresse\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"23 Rue Des Valleries\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Email</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-email-outline text-gray-400 text-lg\"></i></div>
                                        <input pattern=\"(?![_.-])((?![_.-][_.-])[a-zA-Z\\d_.-]){0,63}[a-zA-Z\\d]@((?!-)((?!--)[a-zA-Z\\d-]){0,63}[a-zA-Z\\d]\\.){1,2}([a-zA-Z]{2,14}\\.)?[a-zA-Z]{2,14}\" name=\"email\" type=\"email\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"tholdien@exemple.com\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Mot de passe</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-lock-outline text-gray-400 text-lg\"></i></div>
                                        <!-- Pattern password = 1 - Majuscule / 1 - Minuscule / 1 - Chiffre / 1 - Caractère Spécial (Mini 8 Caractères) -->
                                        <input pattern=\"(?=^.{8,}\$)((?=.*\\d)(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*\$\" name=\"password\" type=\"password\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"************\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Confirmation mot de passe</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-lock-outline text-gray-400 text-lg\"></i></div>
                                        <!-- Pattern password = 1 - Majuscule / 1 - Minuscule / 1 - Chiffre / 1 - Caractère Spécial (Mini 8 Caractères) -->
                                        <input pattern=\"(?=^.{8,}\$)((?=.*\\d)(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*\$\" name=\"passwordC\" type=\"password\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"************\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-2/6 px-3 mb-12\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Choix pays</label>
                                    <select id=\"states\" name=\"pays\" class=\"w-1/5 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5\">
                                        ";
        // line 103
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pays"]) || array_key_exists("pays", $context) ? $context["pays"] : (function () { throw new RuntimeError('Variable "pays" does not exist.', 103, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["cPays"]) {
            // line 104
            echo "                                            <option value=";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cPays"], "codePays", [], "any", false, false, false, 104), "html", null, true);
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cPays"], "nomPays", [], "any", false, false, false, 104), "html", null, true);
            echo "</option>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cPays'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "                                    </select>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <button type=\"submit\" class=\"block w-full max-w-xs mx-auto bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 duration-500 text-white rounded-lg px-3 py-3 font-semibold\">S'INSCRIRE</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "front/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 106,  167 => 104,  163 => 103,  64 => 7,  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"absolute top-24 w-full items-center justify-center\">
        <div class=\"mx-auto bg-gray-100 text-gray-500 rounded-3xl shadow-xl overflow-hidden\" style=\"max-width:1200px\">
            <div class=\"md:flex w-full\">
                <div class=\"w-3/5 py-10 px-10 bg-cover\" style=\"background-image: url('{{ asset('build/static/img/connexion1.jpg') }}')\"></div>
                <div class=\"w-full py-10 px-10 md:px-10\">
                    <div class=\"text-center mb-10\">
                        <h1 class=\"font-bold text-3xl text-gray-900\">THOLDI</h1>
                        <p>Veuillez vous inscrire</p>
                    </div>
                    <form method=\"POST\">
                        <div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Raison sociale</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input pattern=\"[-çéèùa-zA-Z]{3,50}\" name=\"raisonSociale\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"Tholdi\" required>
                                    </div>
                                </div>
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Pseudo</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input pattern=\"[éèa-zA-Z0-9]{3,10}\" name=\"pseudo\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"Tholdien\" required>
                                    </div>
                                </div>
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"pr-2 text-xs font-semibold px-1\">Téléphone</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input pattern=\"[0]{1}[0-9]{9}\" maxlength=\"10\" name=\"telephone\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"0102030405\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Contact</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input name=\"contact\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"Ahri Dupont\">
                                    </div>
                                </div>
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Ville</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input name=\"ville\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"Meulan\" required>
                                    </div>
                                </div>
                                <div class=\"w-1/2 px-3 mb-5\">
                                    <label for=\"\" class=\"pr-2 text-xs font-semibold px-1\">Code Postal</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-account-outline text-gray-400 text-lg\"></i></div>
                                        <input pattern=\"[0-9]{5}\" maxlength=\"5\" name=\"cp\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"78000\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Adresse</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-email-outline text-gray-400 text-lg\"></i></div>
                                        <input name=\"adresse\" type=\"text\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"23 Rue Des Valleries\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Email</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-email-outline text-gray-400 text-lg\"></i></div>
                                        <input pattern=\"(?![_.-])((?![_.-][_.-])[a-zA-Z\\d_.-]){0,63}[a-zA-Z\\d]@((?!-)((?!--)[a-zA-Z\\d-]){0,63}[a-zA-Z\\d]\\.){1,2}([a-zA-Z]{2,14}\\.)?[a-zA-Z]{2,14}\" name=\"email\" type=\"email\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"tholdien@exemple.com\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Mot de passe</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-lock-outline text-gray-400 text-lg\"></i></div>
                                        <!-- Pattern password = 1 - Majuscule / 1 - Minuscule / 1 - Chiffre / 1 - Caractère Spécial (Mini 8 Caractères) -->
                                        <input pattern=\"(?=^.{8,}\$)((?=.*\\d)(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*\$\" name=\"password\" type=\"password\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"************\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Confirmation mot de passe</label>
                                    <div class=\"flex\">
                                        <div class=\"w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center\"><i class=\"mdi mdi-lock-outline text-gray-400 text-lg\"></i></div>
                                        <!-- Pattern password = 1 - Majuscule / 1 - Minuscule / 1 - Chiffre / 1 - Caractère Spécial (Mini 8 Caractères) -->
                                        <input pattern=\"(?=^.{8,}\$)((?=.*\\d)(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*\$\" name=\"passwordC\" type=\"password\" class=\"w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500 duration-500 placeholder-gray-300\" placeholder=\"************\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-2/6 px-3 mb-12\">
                                    <label for=\"\" class=\"text-xs font-semibold px-1\">Choix pays</label>
                                    <select id=\"states\" name=\"pays\" class=\"w-1/5 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5\">
                                        {% for cPays in pays %}
                                            <option value={{ cPays.codePays }}>{{ cPays.nomPays }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                            <div class=\"flex -mx-3\">
                                <div class=\"w-full px-3 mb-5\">
                                    <button type=\"submit\" class=\"block w-full max-w-xs mx-auto bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 duration-500 text-white rounded-lg px-3 py-3 font-semibold\">S'INSCRIRE</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

", "front/register.html.twig", "/var/www/tholdiv2/templates/front/register.html.twig");
    }
}
