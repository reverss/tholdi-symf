<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\AuthenticatorController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\AuthenticatorController::logout'], null, null, null, false, false, null]],
        '/internal/cart/add' => [[['_route' => 'internal_cart_add', '_controller' => 'App\\Controller\\CartController::cartAdd'], null, null, null, false, false, null]],
        '/internal/cart/delete' => [[['_route' => 'internal_cart_cartdelete', '_controller' => 'App\\Controller\\CartController::cartDelete'], null, null, null, false, false, null]],
        '/container' => [[['_route' => 'app_container', '_controller' => 'App\\Controller\\ContainerController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_home', '_controller' => 'App\\Controller\\HomeController::index'], null, null, null, false, false, null]],
        '/profil' => [[['_route' => 'app_profile', '_controller' => 'App\\Controller\\ProfileController::index'], null, null, null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegisterController::index'], null, null, null, false, false, null]],
        '/reservation' => [[['_route' => 'app_reservation', '_controller' => 'App\\Controller\\ReservationController::reservation'], null, null, null, false, false, null]],
        '/internal/reservation/add' => [[['_route' => 'internal_reservationadd', '_controller' => 'App\\Controller\\ReservationController::reservationAdd'], null, null, null, false, false, null]],
        '/stockage' => [[['_route' => 'app_stockage', '_controller' => 'App\\Controller\\StockageController::index'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:35)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        35 => [
            [['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
