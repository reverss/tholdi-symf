<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230331075423 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reserver (id INT AUTO_INCREMENT NOT NULL, code_reservation_id INT DEFAULT NULL, type_container_id INT DEFAULT NULL, quantite_reserver INT NOT NULL, INDEX IDX_B9765E93F30B501D (code_reservation_id), INDEX IDX_B9765E933B624D47 (type_container_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reserver ADD CONSTRAINT FK_B9765E93F30B501D FOREIGN KEY (code_reservation_id) REFERENCES reservation (id)');
        $this->addSql('ALTER TABLE reserver ADD CONSTRAINT FK_B9765E933B624D47 FOREIGN KEY (type_container_id) REFERENCES container (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reserver DROP FOREIGN KEY FK_B9765E93F30B501D');
        $this->addSql('ALTER TABLE reserver DROP FOREIGN KEY FK_B9765E933B624D47');
        $this->addSql('DROP TABLE reserver');
    }
}
