<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230331074112 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, code_pays_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, raison_sociale VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, code_postal VARCHAR(5) NOT NULL, ville VARCHAR(255) NOT NULL, telephone VARCHAR(10) NOT NULL, contact VARCHAR(255) DEFAULT NULL, login VARCHAR(255) NOT NULL, date_last_login DATETIME DEFAULT NULL, date_created DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_C7440455E7927C74 (email), INDEX IDX_C74404559E4306D8 (code_pays_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE container (id INT AUTO_INCREMENT NOT NULL, type_container VARCHAR(4) NOT NULL, libelle_type_container VARCHAR(255) NOT NULL, longueur_cont NUMERIC(5, 0) NOT NULL, largeur_cont NUMERIC(5, 0) NOT NULL, hauteur_cont NUMERIC(5, 0) NOT NULL, volume NUMERIC(3, 0) NOT NULL, poids_cont NUMERIC(5, 0) NOT NULL, capacite_de_charge NUMERIC(8, 2) NOT NULL, tarif_jour NUMERIC(5, 2) NOT NULL, tarif_trim NUMERIC(6, 2) NOT NULL, tarif_ann NUMERIC(7, 2) NOT NULL, photo VARCHAR(255) NOT NULL, photo2 VARCHAR(255) NOT NULL, photo3 VARCHAR(255) NOT NULL, photo4 VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devis (id INT AUTO_INCREMENT NOT NULL, code_devis VARCHAR(5) NOT NULL, date_devis DATETIME NOT NULL, montant_devis NUMERIC(6, 2) NOT NULL, volume NUMERIC(4, 0) NOT NULL, nb_containers INT NOT NULL, valider VARCHAR(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pays (id INT AUTO_INCREMENT NOT NULL, code_pays VARCHAR(4) NOT NULL, nom_pays VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, code_client_id INT DEFAULT NULL, code_devis_id INT DEFAULT NULL, code_ville_mise_dispo_id INT DEFAULT NULL, code_ville_rendre_id INT DEFAULT NULL, date_debut_reservation DATETIME NOT NULL, date_fin_reservation DATETIME NOT NULL, date_reservation DATETIME NOT NULL, volume_estime NUMERIC(4, 0) NOT NULL, etat VARCHAR(1) NOT NULL, INDEX IDX_42C84955B5AE1119 (code_client_id), UNIQUE INDEX UNIQ_42C84955B85F82F1 (code_devis_id), INDEX IDX_42C849554C2CDA0E (code_ville_mise_dispo_id), INDEX IDX_42C8495544086D42 (code_ville_rendre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ville (id INT AUTO_INCREMENT NOT NULL, code_pays_id INT DEFAULT NULL, nom_ville VARCHAR(255) NOT NULL, INDEX IDX_43C3D9C39E4306D8 (code_pays_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404559E4306D8 FOREIGN KEY (code_pays_id) REFERENCES pays (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955B5AE1119 FOREIGN KEY (code_client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955B85F82F1 FOREIGN KEY (code_devis_id) REFERENCES devis (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C849554C2CDA0E FOREIGN KEY (code_ville_mise_dispo_id) REFERENCES ville (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C8495544086D42 FOREIGN KEY (code_ville_rendre_id) REFERENCES ville (id)');
        $this->addSql('ALTER TABLE ville ADD CONSTRAINT FK_43C3D9C39E4306D8 FOREIGN KEY (code_pays_id) REFERENCES pays (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404559E4306D8');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C84955B5AE1119');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C84955B85F82F1');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C849554C2CDA0E');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C8495544086D42');
        $this->addSql('ALTER TABLE ville DROP FOREIGN KEY FK_43C3D9C39E4306D8');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE container');
        $this->addSql('DROP TABLE devis');
        $this->addSql('DROP TABLE pays');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE ville');
    }
}
