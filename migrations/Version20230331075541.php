<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230331075541 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cart (id INT AUTO_INCREMENT NOT NULL, code_client_id INT DEFAULT NULL, type_container_id INT DEFAULT NULL, quantite INT NOT NULL, INDEX IDX_BA388B7B5AE1119 (code_client_id), INDEX IDX_BA388B73B624D47 (type_container_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cart ADD CONSTRAINT FK_BA388B7B5AE1119 FOREIGN KEY (code_client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE cart ADD CONSTRAINT FK_BA388B73B624D47 FOREIGN KEY (type_container_id) REFERENCES container (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cart DROP FOREIGN KEY FK_BA388B7B5AE1119');
        $this->addSql('ALTER TABLE cart DROP FOREIGN KEY FK_BA388B73B624D47');
        $this->addSql('DROP TABLE cart');
    }
}
